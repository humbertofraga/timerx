# Timer +X

Timer +X is an Android app to set an alarm to a fixed time in future.

You can add multiple delays, and click them to set a new alarm. The alarm will
be set on the default clock app.

Timer +X uses app shortcuts to make the access to the timers faster.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/net.xisberto.timerpx/)

![Main screen](fastlane/metadata/android/en-US/images/phone-screenshots/00.png)

![New timer](fastlane/metadata/android/en-US/images/phone-screenshots/01.png)

![Shortcuts](fastlane/metadata/android/en-US/images/phone-screenshots/02.png)
