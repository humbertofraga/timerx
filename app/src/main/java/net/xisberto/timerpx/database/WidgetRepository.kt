package net.xisberto.timerpx.database

class WidgetRepository(private val database: Database) {

    fun get(appWidgetId: Int) = database.widgetDao().get(appWidgetId)

    fun insert(timerWidget: TimerWidget): Long = database.widgetDao().insert(timerWidget)

    fun delete(timerWidget: TimerWidget) = database.widgetDao().delete(timerWidget)

    fun delete(appWidgetId: Int) {
        val widgetAndDef = get(appWidgetId) ?: return
        database.widgetDao().delete(checkNotNull(widgetAndDef.widget))
    }
}