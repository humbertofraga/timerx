package net.xisberto.timerpx.database

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@androidx.room.Database(
    entities = [TimerDef::class, TimerWidget::class],
    version = 3,
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
        AutoMigration(from = 2, to = 3)
    ],
    exportSchema = true
)
abstract class Database : RoomDatabase() {

    abstract fun timerDao(): TimerDao
    abstract fun widgetDao(): TimerWidgetDao

    companion object {

        @Volatile
        private var INSTANCE: Database? = null

        fun getDatabase(context: Context): Database {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    Database::class.java,
                    "database"
                )
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                    .build()
                INSTANCE = instance
                return instance
            }
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE TimerDef ADD COLUMN label TEXT NOT NULL DEFAULT 'Timer +X'")
            }
        }

        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE TimerDef RENAME TO TimerDefMigration")
                database.execSQL(
                    "CREATE TABLE `TimerDef` (" +
                            "`id` INTEGER PRIMARY KEY NOT NULL DEFAULT 0," +
                            " `duration` INTEGER NOT NULL," +
                            " `label` TEXT NOT NULL DEFAULT 'Timer +X')"
                )
                database.execSQL("INSERT INTO TimerDef(`duration`,`label`) SELECT * FROM TimerDefMigration")
                database.execSQL("DROP TABLE TimerDefMigration")
                database.execSQL(
                    "CREATE TABLE `TimerWidget` (" +
                            "`id` INTEGER PRIMARY KEY NOT NULL," +
                            "`appWidgetId` INTEGER NOT NULL," +
                            " `timerDefId` INTEGER NOT NULL" +
                            " REFERENCES `TimerDef`(`id`)" +
                            " ON DELETE CASCADE)"
                )
                database.execSQL("CREATE INDEX 'index_TimerWidget_timerDefId'" +
                        "ON `TimerWidget`(`timerDefId`)")
            }
        }
    }
}