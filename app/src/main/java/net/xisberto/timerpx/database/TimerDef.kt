package net.xisberto.timerpx.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TimerDef(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(defaultValue = "0") val id: Int = 0,
    val duration: Long,
    @ColumnInfo(defaultValue = "Timer +X") val label: String = "Timer +X"
)