package net.xisberto.timerpx.timer_list

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import net.xisberto.timerpx.database.TimerDef
import net.xisberto.timerpx.databinding.ItemTimerBinding
import net.xisberto.timerpx.util.formatTime

/**
 * [RecyclerView.Adapter] that can display a list of [TimerDef].
 */
class TimerDefRecyclerViewAdapter(
    private val values: List<TimerDef>,
    private val mode: Int = TimerDefFragment.ACTION_MANAGE,
    private val onTimerInteractionListener: OnTimerInteractionListener
) : RecyclerView.Adapter<TimerDefRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemTimerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, mode)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.itemView.tag = item
        holder.bind(item, onTimerInteractionListener)
        Log.d("ViewAdapter", holder.toString())
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: ItemTimerBinding, val mode: Int) :
        RecyclerView.ViewHolder(binding.root) {
        private val duration: TextView = binding.duration
        private val label: TextView = binding.label
        private val btnRemove: Button = binding.btnRemove

        fun bind(timerDef: TimerDef, onTimerInteractionListener: OnTimerInteractionListener) {
            duration.text = formatTime(timerDef)
            label.text = timerDef.label
            itemView.setOnClickListener { onTimerInteractionListener.onTimerClicked(timerDef) }
            when (mode) {
                TimerDefFragment.ACTION_MANAGE -> btnRemove.setOnClickListener {
                    onTimerInteractionListener.onTimerRemovedClicked(
                        timerDef
                    )
                }
                TimerDefFragment.ACTION_SELECT -> btnRemove.visibility = View.GONE
            }
        }

        override fun toString(): String {
            return super.toString() + " ${duration.text}"
        }
    }

    interface OnTimerInteractionListener {
        fun onTimerClicked(timerDef: TimerDef)
        fun onTimerRemovedClicked(timerDef: TimerDef)
    }
}