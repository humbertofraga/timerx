package net.xisberto.timerpx.timer_list

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import net.xisberto.timerpx.R
import net.xisberto.timerpx.databinding.DialogNewTimerBinding

class DurationPickerDialog : DialogFragment() {

    private var _binding: DialogNewTimerBinding? = null
    private val binding get() = _binding!!
    private lateinit var newTimerViewModel: NewTimerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let {
            newTimerViewModel = ViewModelProvider(it)[NewTimerViewModel::class.java]
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { it ->
            _binding = DialogNewTimerBinding.inflate(it.layoutInflater)
            binding.timePicker.setIs24HourView(true)
            binding.timePicker.hour = newTimerViewModel.hour.value ?: 1
            binding.timePicker.minute = newTimerViewModel.minute.value ?: 0
            binding.timePicker.setOnTimeChangedListener { _, hour, minute ->
                newTimerViewModel.hour.postValue(hour)
                newTimerViewModel.minute.postValue(minute)
            }
            binding.editLabel.setText(newTimerViewModel.label.value ?: getString(R.string.timer_x))
            val dialog = AlertDialog.Builder(it)
                .setTitle(R.string.title_select_duration)
                .setView(binding.root)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.cancel()
                }
                .create()
            dialog.setOnShowListener(showListener)
            return dialog
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private val showListener = DialogInterface.OnShowListener {
        dialog?.let { dlg ->
            (dlg as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener {
                val label = binding.editLabel.text.toString()
                Log.d("Dialog", "name is empty: ${label.isEmpty()}")
                if (label.isNotEmpty()) {
                    newTimerViewModel.hour.postValue(binding.timePicker.hour)
                    newTimerViewModel.minute.postValue(binding.timePicker.minute)
                    newTimerViewModel.label.postValue(label)
                    dlg.dismiss()
                } else {
                    binding.editLabel.error = getString(R.string.error_name_empty)
                }
            }
        }
    }
}