package net.xisberto.timerpx.timer_list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class NewTimerViewModel(application: Application) : AndroidViewModel(application) {
    val hour = MutableLiveData<Int>()
    val minute = MutableLiveData<Int>()
    val label = MutableLiveData<String>()
}