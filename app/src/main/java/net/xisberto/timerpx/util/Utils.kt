package net.xisberto.timerpx.util

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.os.Build
import android.provider.AlarmClock
import net.xisberto.timerpx.database.TimerDef
import java.util.*

fun getAlarmApps(context: Context): List<ResolveInfo> {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        context.packageManager.queryIntentActivities(
            Intent(AlarmClock.ACTION_SET_ALARM),
            PackageManager.ResolveInfoFlags.of(PackageManager.MATCH_ALL.toLong())
        )
    } else {
        context.packageManager.queryIntentActivities(
            Intent(AlarmClock.ACTION_SET_ALARM),
            PackageManager.MATCH_ALL
        )
    }
}

fun hasAlarmApps(context: Context): Boolean {
    return getAlarmApps(context).isNotEmpty()
}

fun formatTime(timerDef: TimerDef): String {
    return formatTime(timerDef.duration)
}

fun formatTime(duration: Long): String {
    return String.format(
        Locale.getDefault(),
        "%02d:%02d",
        duration / 3600,
        (duration % 3600) / 60
    )
}

fun formatTime(calendar: Calendar): String {
    return String.format(
        Locale.getDefault(),
        "%02d:%02d",
        calendar.get(Calendar.HOUR_OF_DAY),
        calendar.get(Calendar.MINUTE)
    )
}