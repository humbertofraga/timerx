package net.xisberto.timerpx.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import androidx.preference.Preference
import androidx.preference.Preference.SummaryProvider
import androidx.preference.PreferenceFragmentCompat
import com.mikepenz.aboutlibraries.LibsBuilder
import net.xisberto.timerpx.BuildConfig
import net.xisberto.timerpx.R
import net.xisberto.timerpx.databinding.SettingsActivityBinding

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = SettingsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.settings, SettingsFragment())
            }
        }
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            if (supportFragmentManager.fragments[0] is SettingsFragment) {
                finish()
            } else {
                supportFragmentManager.popBackStack()
            }
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            val aboutPreference: Preference? = findPreference(getString(R.string.key_show_about))
            aboutPreference?.summaryProvider = SummaryProvider<Preference> {
                "Version " + BuildConfig.VERSION_NAME
            }
        }

        override fun onPreferenceTreeClick(preference: Preference): Boolean {
            return if (preference.key == getString(R.string.key_show_about)) {
                val fragment = LibsBuilder()
                    .withAboutIconShown(true)
                    .withAboutVersionShownName(true)
                    .withActivityTitle(getString(R.string.app_name))
                    .withAboutDescription(getString(R.string.text_about))
                    .supportFragment()
                parentFragmentManager.commit {
                    setReorderingAllowed(true)
                    addToBackStack("")
                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    replace(R.id.settings, fragment)
                }
                true
            } else if (preference.key == getString(R.string.show_alarm_apps)) {
                val fragment = CompatibleAppsFragment()
                parentFragmentManager.commit {
                    setReorderingAllowed(true)
                    addToBackStack("")
                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    replace(R.id.settings, fragment)
                }
                true
            } else {
                false
            }
        }
    }
}