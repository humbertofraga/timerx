package net.xisberto.timerpx.settings

import android.content.pm.ResolveInfo
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import net.xisberto.timerpx.databinding.ItemCompatibleAppsBinding

class CompatibleAppsRecyclerViewAdapter(
    private val values: List<ResolveInfo>
) : RecyclerView.Adapter<CompatibleAppsRecyclerViewAdapter.ViewHolder>() {

    init {
        Log.i("Adapter", "Creating adapter with ${values.size} items")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemCompatibleAppsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(values[position])
    }

    inner class ViewHolder(private val binding: ItemCompatibleAppsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(resolveInfo: ResolveInfo) {
            val pm = binding.root.context.packageManager
            val resources = pm.getResourcesForApplication(resolveInfo.activityInfo.applicationInfo)
            val appName = if (resolveInfo.labelRes != 0) {
                resources.getString(resolveInfo.labelRes)
            } else {
                resolveInfo.loadLabel(pm).toString()
            }
            val packageName = resolveInfo.activityInfo.packageName

            Log.i("Adapter", "Binding for $packageName")

            binding.text1.text = appName
            binding.text2.text = packageName
        }
    }
}