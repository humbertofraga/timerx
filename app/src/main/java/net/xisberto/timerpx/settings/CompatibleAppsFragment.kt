package net.xisberto.timerpx.settings

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import net.xisberto.timerpx.databinding.FragmentCompatibleAppsBinding
import net.xisberto.timerpx.util.getAlarmApps

class CompatibleAppsFragment: Fragment() {

    private var _binding: FragmentCompatibleAppsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCompatibleAppsBinding.inflate(inflater, container, false)
        val view = binding.root

        val alarmApps = getAlarmApps(view.context)
        Log.i("", "Found ${alarmApps.size} apps")
        alarmApps.forEach {
            Log.i("", "- ${it.activityInfo.packageName}")
        }

        binding.list.adapter = CompatibleAppsRecyclerViewAdapter(getAlarmApps(view.context))
        return view
    }
}