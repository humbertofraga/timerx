package net.xisberto.timerpx.appwidget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.BroadcastReceiver
import android.content.Context
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import net.xisberto.timerpx.R
import net.xisberto.timerpx.database.Database
import net.xisberto.timerpx.database.WidgetAndDef
import net.xisberto.timerpx.database.WidgetRepository
import net.xisberto.timerpx.timer_list.getIntent
import net.xisberto.timerpx.util.formatTime
import kotlin.coroutines.CoroutineContext

private const val TAG = "TimerAppWidget"

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in [TimerAppWidgetConfigureActivity]
 */
class TimerAppWidget : AppWidgetProvider() {

    private fun BroadcastReceiver.goAsync(
        context: CoroutineContext = Dispatchers.IO,
        block: suspend CoroutineScope.() -> Unit
    ) {
        val pendingResult = goAsync()
        CoroutineScope(SupervisorJob()).launch(context) {
            try {
                block()
            } finally {
                pendingResult.finish()
            }
        }
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) = goAsync {
        val repository = WidgetRepository(Database.getDatabase(context))
        for (appWidgetId in appWidgetIds) {
            Log.d(TAG, "System is updating appwidget $appWidgetId")
            repository.get(appWidgetId)?.let {
                updateAppWidget(context, appWidgetManager, it)
            } ?: run {
                Log.d(TAG, "Widget not found in database")
            }
        }
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) = goAsync {
        // When the user deletes the widget, delete the preference associated with it.
        val repository = WidgetRepository(Database.getDatabase(context))
        for (appWidgetId in appWidgetIds) {
            Log.d(TAG, "Deleting AppWidget $appWidgetId")
            repository.delete(appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager = AppWidgetManager.getInstance(context),
    widgetAndDef: WidgetAndDef
) {
    Log.d(TAG, "Updating widget ${widgetAndDef.widget.appWidgetId}")
    val appWidgetText =
        "${formatTime(widgetAndDef.timerDef.duration)} ${widgetAndDef.timerDef.label}"
    Log.d(TAG, appWidgetText)
    val views = RemoteViews(context.packageName, R.layout.timer_app_widget)
    views.setTextViewText(R.id.appwidget_text, appWidgetText)
    views.setContentDescription(R.id.appwidget_icon, formatTime(widgetAndDef.timerDef.duration))
    val flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        PendingIntent.FLAG_MUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    } else {
        PendingIntent.FLAG_UPDATE_CURRENT
    }
    views.setOnClickPendingIntent(
        R.id.appwidget_widget,
        PendingIntent.getActivity(
            context,
            0,
            getIntent(context, widgetAndDef.timerDef),
            flags
        )
    )
    appWidgetManager.updateAppWidget(widgetAndDef.widget.appWidgetId, views)
}