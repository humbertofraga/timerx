package net.xisberto.timerpx.appwidget

import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import net.xisberto.timerpx.R
import net.xisberto.timerpx.database.TimerViewModel
import net.xisberto.timerpx.database.TimerWidget
import net.xisberto.timerpx.database.WidgetViewModel
import net.xisberto.timerpx.databinding.TimerAppWidgetConfigureBinding
import net.xisberto.timerpx.timer_list.TimerDefFragment

private const val TAG = "AppWidgetConfiguration"

/**
 * The configuration screen for the [TimerAppWidget] AppWidget.
 */
class TimerAppWidgetConfigureActivity : AppCompatActivity() {
    private var appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private val timerViewModel: TimerViewModel by viewModels()
    private val widgetViewModel: WidgetViewModel by viewModels()
    private lateinit var binding: TimerAppWidgetConfigureBinding

    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED)

        // Find the widget id from the intent.
        val intent = intent
        val extras = intent.extras
        if (extras != null) {
            appWidgetId = extras.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
            )
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
            return
        }

        binding = TimerAppWidgetConfigureBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.d(TAG, "Starting configuration activity")

        if (icicle == null) {
            supportFragmentManager.commit {
                replace(
                    R.id.main_content,
                    TimerDefFragment.newInstance(TimerDefFragment.ACTION_SELECT)
                )
            }
        }

        timerViewModel.selection.observe(this) { timerDef ->
            Log.d(TAG, "Selected ${timerDef.id}")

            widgetViewModel.newId.observe(this) { id ->
                Log.d(TAG, "Repository created widget with id $id")
                widgetViewModel.get(id.toInt()).observe(this) {
                    Log.d(TAG, "Got widgetAndDef ${it.widget.appWidgetId}")
                    CoroutineScope(SupervisorJob()).launch(Dispatchers.IO) {
                        updateAppWidget(this@TimerAppWidgetConfigureActivity, widgetAndDef = it)
                        // Make sure we pass back the original appWidgetId
                        val resultValue = Intent()
                        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
                        setResult(RESULT_OK, resultValue)
                        finish()
                    }
                }
            }
            widgetViewModel.insert(TimerWidget(timerDefId = timerDef.id, appWidgetId = appWidgetId))
        }
    }

}
