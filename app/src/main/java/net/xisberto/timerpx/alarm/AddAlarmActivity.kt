package net.xisberto.timerpx.alarm

import android.content.Intent
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.AlarmClock
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.preference.PreferenceManager
import net.xisberto.timerpx.MainActivity
import net.xisberto.timerpx.R
import net.xisberto.timerpx.databinding.ActivityAddAlarmBinding
import net.xisberto.timerpx.util.formatTime
import net.xisberto.timerpx.util.hasAlarmApps
import java.util.*

class AddAlarmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)

        if (hasAlarmApps(this)) {
            val delay: Long = intent.getLongExtra(EXTRA_DELAY, 300)
            val label = intent.getStringExtra(EXTRA_LABEL)

            val delayHour = delay / 3600
            val delayMinute = (delay % 3600) / 60
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.HOUR_OF_DAY, delayHour.toInt())
            calendar.add(Calendar.MINUTE, delayMinute.toInt())

            setAlarm(calendar, label)

            val settings = PreferenceManager.getDefaultSharedPreferences(this)
            if (!(settings.contains(getString(R.string.key_show_alarm)) and
                        settings.getBoolean(getString(R.string.key_show_alarm), true))
            ) {
                finish()
            }

            val binding = ActivityAddAlarmBinding.inflate(layoutInflater)
            setContentView(binding.root)
            setSupportActionBar(binding.toolbar)
            binding.toolbar.setNavigationOnClickListener {
                finish()
            }
            val animation = binding.toolbar.navigationIcon as AnimatedVectorDrawable
            animation.registerAnimationCallback(object : Animatable2.AnimationCallback() {
                override fun onAnimationEnd(drawable: Drawable?) {
                    super.onAnimationEnd(drawable)
                    finish()
                }
            })
            animation.start()
            binding.message.text = getString(R.string.alarm_added, label)
            binding.time.text = formatTime(calendar)
            binding.buttonClock.setOnClickListener {
                startActivity(
                    Intent(AlarmClock.ACTION_SHOW_ALARMS)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
            binding.buttonDontShowAgain.setOnClickListener {
                settings.edit()
                    .putBoolean(getString(R.string.key_show_alarm), false)
                    .apply()
                finish()
            }
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun setAlarm(calendar: Calendar, label: String?) {
        startActivity(
            Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_HOUR, calendar.get(Calendar.HOUR_OF_DAY))
                .putExtra(AlarmClock.EXTRA_MINUTES, calendar.get(Calendar.MINUTE))
                .putExtra(AlarmClock.EXTRA_MESSAGE, label)
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        )
    }

    companion object {
        const val EXTRA_DELAY = "delay"
        const val EXTRA_LABEL = "label"
    }
}