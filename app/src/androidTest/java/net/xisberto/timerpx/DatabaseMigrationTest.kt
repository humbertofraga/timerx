package net.xisberto.timerpx

import androidx.room.testing.MigrationTestHelper
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import net.xisberto.timerpx.database.Database
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class DatabaseMigrationTest {
    private val mTESTDATABASE = "migration-test"

    @get:Rule
    val helper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        Database::class.java
    )

    @Test
    @kotlin.jvm.Throws(IOException::class)
    fun migrate1To2() {
        helper.createDatabase(mTESTDATABASE, 1).apply {
            execSQL("INSERT INTO TimerDef VALUES (3600)")
            close()
        }

        helper.runMigrationsAndValidate(mTESTDATABASE, 2, true, Database.MIGRATION_1_2).apply {
            val cursor = query("SELECT * FROM TimerDef WHERE duration = 3600")
            cursor.moveToFirst()
            assertTrue(cursor.getLong(0) == 3600L)
            close()
        }
    }

    @Test
    @kotlin.jvm.Throws(IOException::class)
    fun migrate2To3() {
        helper.createDatabase(mTESTDATABASE, 2).apply {
            execSQL("INSERT INTO TimerDef('duration') VALUES (6000)")
            close()
        }

        helper.runMigrationsAndValidate(mTESTDATABASE, 3, true, Database.MIGRATION_2_3).apply {
            execSQL("INSERT INTO TimerWidget('timerDefId', 'appWidgetId') VALUES (1,3)")
            val c1 = query("SELECT * FROM TimerDef WHERE duration = 6000")
            c1.moveToFirst()
            assertTrue(c1.getInt(1) == 6000)
            val c2 = query(
                "SELECT d.id did,d.duration,w.appWidgetId wid,w.timerDefId FROM TimerWidget w" +
                        " JOIN TimerDef d ON w.timerDefId = d.id" +
                        " WHERE w.timerDefId = 1"
            )
            c2.moveToFirst()
            println("cursor with ${c2.columnCount} columns")
            for (i in 0 until c2.columnCount) {
                println("$i - ${c2.getColumnName(i)} - ${c2.getInt(i)}")
            }
            close()
        }
    }
}