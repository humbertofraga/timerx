## 1.4.2

- Corrects the sort icon color on dark theme
- Corrects listing of alarm apps
- Shows compatible alarm apps on Settings
